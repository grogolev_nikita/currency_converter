package com.thumbtack.converter.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.FetchType;

import com.thumbtack.converter.dao.CurrenciesRateDao;
import com.thumbtack.converter.entities.CurrenciesRate;
import com.thumbtack.converter.entities.Currency;

public interface CurrenciesRateMapper {
	final String selectAllQuery = "SELECT * FROM currencies_rate";
	
	final String selectByIdQuery = "SELECT * FROM currencies_rate WHERE id=#{id}";
	
	final String insertOrUpdateQuery = "INSERT INTO currencies_rate (id, iso_4217, name) VALUES (#{id}, #{iso}, #{name})"
			+ " ON DUPLICATE KEY UPDATE iso_427=#{iso}, name=#{name}";
	
	final String selectByFromCurrencyIdQuery = "SELECT * FROM currencies_rate WHERE from_currency_id=#{id}";
	
	@Select(selectAllQuery)
	@Results({
		@Result(property="id", column="id", id=true),
		@Result(property="from", column="from_currency_id", javaType=Currency.class,
				one=@One(select = "com.thumbtack.converter.mappers.CurrencyMapper.selectById", fetchType = FetchType.LAZY)),
		@Result(property="to", column="to_currency_id", javaType=Currency.class,
				one=@One(select = "com.thumbtack.converter.mappers.CurrencyMapper.selectById", fetchType = FetchType.LAZY)),
		@Result(property="rate", column="rate")
	})
	List<CurrenciesRate> selectAll();
	
	@Select(selectByFromCurrencyIdQuery)
	@Results({
		@Result(property="id", column="id", id=true),
		@Result(property="from", column="from_currency_id", javaType=Currency.class,
				one=@One(select = "com.thumbtack.converter.mappers.CurrencyMapper.selectById", fetchType = FetchType.LAZY)),
		@Result(property="to", column="to_currency_id", javaType=Currency.class,
				one=@One(select = "com.thumbtack.converter.mappers.CurrencyMapper.selectById", fetchType = FetchType.LAZY)),
		@Result(property="rate", column="rate")
	})
	List<CurrenciesRate> selectByFromCurrencyId(Integer id);
	
	@Select(selectByIdQuery)
	@Results({
		@Result(property="id", column="id", id=true),
		@Result(property="from", column="from_currency_id", javaType=Currency.class,
				one=@One(select = "com.thumbtack.converter.mappers.CurrencyMapper.selectById", fetchType = FetchType.LAZY)),
		@Result(property="to", column="to_currency_id", javaType=Currency.class,
				one=@One(select = "com.thumbtack.converter.mappers.CurrencyMapper.selectById", fetchType = FetchType.LAZY)),
		@Result(property="rate", column="rate")
	})
	CurrenciesRate selectById(Integer id);
	
	@Insert({ "<script>",
		"INSERT INTO currencies_rate (id, from_currency_id, to_currency_id, rate) VALUES",
		"<foreach item='item' collection='list' separator=','>",
		"(#{item.id}, #{item.from.id}, #{item.to.id}, #{item.rate})",
		"</foreach>",
		"ON DUPLICATE KEY UPDATE "
		+ "id=VALUES(id), from_currency_id=VALUES(from_currency_id), to_currency_id=VALUES(to_currency_id), "
		+ "rate=VALUES(rate)",
		"</script>" })
	@Options(useGeneratedKeys=true)
	void batchInsertOrUpdate(List<CurrenciesRate> rates);
}
