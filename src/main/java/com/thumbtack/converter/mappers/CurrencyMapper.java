package com.thumbtack.converter.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.thumbtack.converter.entities.Currency;

public interface CurrencyMapper {
	final String selectAllQuery = "SELECT * FROM currencies";
	
	final String selectByIsoQuery = "SELECT * FROM currencies WHERE iso_4217=#{iso}";
	
	final String selectByIdQuery = "SELECT * FROM currencies WHERE id=#{id}";
	
	final String insertOrUpdateQuery = "INSERT INTO currencies (id, iso_4217, name) VALUES (#{id}, #{iso}, #{name})"
			+ " ON DUPLICATE KEY UPDATE iso_427=#{iso}, name=#{name}";
	
	@Select(selectAllQuery)
	@Results({
		@Result(property="id", column="id", id=true),
		@Result(property="iso", column="iso_4217"),
		@Result(property="name", column="name")
	})
	List<Currency> selectAll();
	
	@Select(selectByIsoQuery)
	@Results({
		@Result(property="id", column="id", id=true),
		@Result(property="iso", column="iso_4217"),
		@Result(property="name", column="name")
	})
	Currency selectByIso(String iso);
	
	@Select(selectByIdQuery)
	@Results({
		@Result(property="id", column="id", id=true),
		@Result(property="iso", column="iso_4217"),
		@Result(property="name", column="name")
	})
	Currency selectById(Integer id);
	
	
	@Insert(insertOrUpdateQuery)
	@Options(useGeneratedKeys=true)
	void insertOrUpdate(Currency currecy);
	
	@Insert({ "<script>",
		"INSERT INTO currencies (id, iso_4217, name) VALUES",
		"<foreach item='item' collection='list' separator=','>",
		"(#{item.id}, #{item.iso}, #{item.name})",
		"</foreach>",
		"ON DUPLICATE KEY UPDATE "
		+ "id=VALUES(id), iso_4217=VALUES(iso_4217), name=VALUES(name)",
		"</script>" })
	@Options(useGeneratedKeys=true)
	void batchInsertOrUpdate(List<Currency> currencies);
}
