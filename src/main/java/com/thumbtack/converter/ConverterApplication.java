package com.thumbtack.converter;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.thumbtack.converter.exceptions.FatalConverterException;
import com.thumbtack.converter.utils.MyBatisUtils;

@SpringBootApplication
@EnableScheduling
public class ConverterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConverterApplication.class, args);
	}
	
	@PostConstruct
	private void init() throws FatalConverterException {
		if (!MyBatisUtils.initSqlSessionFactory())
			throw new FatalConverterException("Unable to initialize database session");
	}
}
