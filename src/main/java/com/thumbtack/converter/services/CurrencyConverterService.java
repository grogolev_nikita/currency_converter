package com.thumbtack.converter.services;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.AllDirectedPaths;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedWeightedPseudograph;
import org.jgrapht.util.WeightCombiner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thumbtack.converter.dao.CurrenciesRateDao;
import com.thumbtack.converter.dao.CurrencyDao;
import com.thumbtack.converter.dto.output.ConversionPathDto;
import com.thumbtack.converter.dto.output.CurrenciesRateDto;
import com.thumbtack.converter.dto.output.CurrencyDto;
import com.thumbtack.converter.entities.CurrenciesRate;
import com.thumbtack.converter.entities.Currency;
import com.thumbtack.converter.exceptions.ConverterException;

@Service
public class CurrencyConverterService {
	@Autowired
	private CurrencyDao currencyDao;

	@Autowired
	private CurrenciesRateDao currenciesRateDao;

	private final String defaultBaseIso = "USD";

	private DirectedWeightedPseudograph<Currency, DefaultWeightedEdge> currencyGraph;

	public List<CurrenciesRateDto> getLatestCurrencyRates(String baseCurrencyIso) throws ConverterException {
		Currency baseCurrency = baseCurrencyIso == null ? currencyDao.getByIso(defaultBaseIso)
				: currencyDao.getByIso(baseCurrencyIso);

		return convertToCurrenciesRateDtoList(currenciesRateDao.getByBaseCurrencyId(baseCurrency.getId()));
	}

	public List<CurrencyDto> getAllCurrencies() {
		return convertToCurrencyDtoList(currencyDao.getAll());
	}

	public void updateSymbols(List<CurrencyDto> symbols) {
		List<Currency> currencies = convertToCurrencyEntityList(symbols);
		currencyDao.batchInsertOrUpdate(currencies);
	}

	public void updateRates(List<CurrenciesRateDto> currenciesRates) {
		List<CurrenciesRate> rates = convertToCurrenciesRateEntityList(currenciesRates);
		currenciesRateDao.batchInsertOrUpdate(rates);
	}

	public ConversionPathDto getOprimalConversionPath(String fromIso, String toIso, Integer amount, Integer hops) {
		Currency from = currencyDao.getByIso(fromIso);
		Currency to = currencyDao.getByIso(toIso);

		AllDirectedPaths<Currency, DefaultWeightedEdge> adp = new AllDirectedPaths(currencyGraph);
		List<GraphPath<Currency, DefaultWeightedEdge>> paths = adp.getAllPaths(from, to, true, hops);

		GraphPath<Currency, DefaultWeightedEdge> optimalPath = paths.stream()
				.max(new Comparator<GraphPath<Currency, DefaultWeightedEdge>>() {

					@Override
					public int compare(GraphPath<Currency, DefaultWeightedEdge> p1,
							GraphPath<Currency, DefaultWeightedEdge> p2) {

						double firstPathMuliplier = p1.getEdgeList().stream()
								.map(a -> currencyGraph.getEdgeWeight(a))
								.reduce((a, b) -> a * b).get();

						double secondPathMultiplier = p2.getEdgeList().stream()
								.map(a -> currencyGraph.getEdgeWeight(a))
								.reduce((a, b) -> a * b).get();

						return Double.compare(firstPathMuliplier, secondPathMultiplier);
					}

				}).get();

		return convertGraphPathToConversionPathDto(optimalPath, from, to, amount);
	}

	public CurrenciesRateDto getConversionRate(String fromIso, String toIso, Integer amount) {
		Currency from = currencyDao.getByIso(fromIso);
		Currency to = currencyDao.getByIso(toIso);
		Double rate = currencyGraph.getEdgeWeight(currencyGraph.getEdge(from, to));

		return new CurrenciesRateDto(fromIso, toIso, rate);
	}

	/* Converters */

	private ConversionPathDto convertGraphPathToConversionPathDto(GraphPath path, Currency from, Currency to,
			Integer amount) {

		List rates = new ArrayList<CurrenciesRateDto>();
		Double conversionResult = 1.0;

		List<Currency> vertexList = path.getVertexList();
		for (int i = 0; i < vertexList.size() - 1; i++) {
			Currency currentVertex = vertexList.get(i);
			Currency nextVertex = vertexList.get(i + 1);

			Double localRate = currencyGraph.getEdgeWeight(currencyGraph.getEdge(currentVertex, nextVertex));

			rates.add(new CurrenciesRateDto(currentVertex.getIso(), nextVertex.getIso(), localRate));

			conversionResult *= localRate;
		}

		conversionResult *= amount;

		return new ConversionPathDto(from.getIso(), to.getIso(), amount, rates, conversionResult);
	}

	private List<CurrenciesRate> convertToCurrenciesRateEntityList(List<CurrenciesRateDto> currenciesRates) {
		List currencies = new ArrayList();

		for (CurrenciesRateDto dto : currenciesRates) {
			currencies.add(convertToCurrenciesRateEntity(dto));
		}

		return currencies;
	}

	private List<CurrenciesRateDto> convertToCurrenciesRateDtoList(List<CurrenciesRate> currenciesRates) {
		List dtos = new ArrayList();

		for (CurrenciesRate entity : currenciesRates) {
			dtos.add(convertToCurrenciesRateDto(entity));
		}

		return dtos;
	}

	private CurrenciesRateDto convertToCurrenciesRateDto(CurrenciesRate entity) {
		return new CurrenciesRateDto(entity.getFrom().getIso(), entity.getTo().getIso(), entity.getRate());
	}

	private CurrenciesRate convertToCurrenciesRateEntity(CurrenciesRateDto dto) {
		Currency fromCurrency = currencyDao.getByIso(dto.getFrom());
		Currency toCurrency = currencyDao.getByIso(dto.getTo());

		return new CurrenciesRate(dto.getId(), fromCurrency, toCurrency, dto.getRate());
	}

	private Currency convertToCurrencyEntity(CurrencyDto dto) {
		return new Currency(dto.getId(), dto.getIso(), dto.getName());
	}

	private List<Currency> convertToCurrencyEntityList(List<CurrencyDto> symbols) {
		List currencies = new ArrayList();

		for (CurrencyDto dto : symbols) {
			currencies.add(convertToCurrencyEntity(dto));
		}

		return currencies;
	}

	private List<CurrencyDto> convertToCurrencyDtoList(List<Currency> entities) {
		List dtos = new ArrayList();

		for (Currency entity : entities) {
			dtos.add(convertToCurrencyDto(entity));
		}

		return dtos;
	}

	private CurrencyDto convertToCurrencyDto(Currency entity) {
		return new CurrencyDto(entity.getId(), entity.getIso(), entity.getName());
	}

	public void rebuildCurrencyGraph() {
		DirectedWeightedPseudograph<Currency, DefaultWeightedEdge> newCurrencyGraph = new DirectedWeightedPseudograph<>(DefaultWeightedEdge.class);

		List<CurrenciesRate> rates = currenciesRateDao.getAll();

		for (CurrenciesRate rate : rates) {
			newCurrencyGraph.addVertex(rate.getFrom());

			newCurrencyGraph.addVertex(rate.getTo());

			DefaultWeightedEdge rateEdge = newCurrencyGraph.addEdge(rate.getFrom(), rate.getTo());

			newCurrencyGraph.setEdgeWeight(rateEdge, rate.getRate());
		}
		
		currencyGraph = newCurrencyGraph;
	}
}
