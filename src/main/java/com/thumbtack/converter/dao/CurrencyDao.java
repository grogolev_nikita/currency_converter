package com.thumbtack.converter.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;

import com.thumbtack.converter.daoint.ICurrencyDao;
import com.thumbtack.converter.entities.Currency;
import com.thumbtack.converter.mappers.CurrencyMapper;

@Component
public class CurrencyDao extends BaseDao implements ICurrencyDao {

	@Override
	public List<Currency> getAll() {
		List<Currency> currencies = new ArrayList();
		try (SqlSession sqlSession = getSession()) {
			try {
				currencies = sqlSession.getMapper(CurrencyMapper.class).selectAll();
			} catch (RuntimeException ex) {
				sqlSession.rollback();
				throw ex;
			}
			sqlSession.commit();
		}

		return currencies;
	}

	@Override
	public void batchInsertOrUpdate(List<Currency> currencies) {
		try (SqlSession sqlSession = getSession()) {
			try {
				sqlSession.getMapper(CurrencyMapper.class).batchInsertOrUpdate(currencies);
			} catch (RuntimeException ex) {
				sqlSession.rollback();
				throw ex;
			}
			sqlSession.commit();
		}
	}

	@Override
	public Currency getByIso(String iso) {
		Currency currency = null;
		try (SqlSession sqlSession = getSession()) {
			try {
				currency = sqlSession.getMapper(CurrencyMapper.class).selectByIso(iso);
			} catch (RuntimeException ex) {
				sqlSession.rollback();
				throw ex;
			}
			sqlSession.commit();
		}

		return currency;
	}

}
