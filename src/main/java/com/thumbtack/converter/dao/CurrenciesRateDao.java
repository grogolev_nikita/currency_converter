package com.thumbtack.converter.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;

import com.thumbtack.converter.daoint.ICurrenciesRateDao;
import com.thumbtack.converter.entities.CurrenciesRate;
import com.thumbtack.converter.entities.Currency;
import com.thumbtack.converter.mappers.CurrenciesRateMapper;
import com.thumbtack.converter.mappers.CurrencyMapper;

@Component
public class CurrenciesRateDao extends BaseDao implements ICurrenciesRateDao {

	@Override
	public List<CurrenciesRate> getAll() {
		List<CurrenciesRate> rates = new ArrayList();
		try (SqlSession sqlSession = getSession()) {
			try {
				rates = sqlSession.getMapper(CurrenciesRateMapper.class).selectAll();
			} catch (RuntimeException ex) {
				sqlSession.rollback();
				throw ex;
			}
			sqlSession.commit();
		}

		return rates;
	}

	@Override
	public void batchInsertOrUpdate(List<CurrenciesRate> rates) {
		try (SqlSession sqlSession = getSession()) {
			try {
				sqlSession.getMapper(CurrenciesRateMapper.class).batchInsertOrUpdate(rates);
			} catch (RuntimeException ex) {
				sqlSession.rollback();
				throw ex;
			}
			sqlSession.commit();
		}
	}

	public List<CurrenciesRate> getByBaseCurrencyId(Integer id) {
		List currenciesRates = new ArrayList();
		try (SqlSession sqlSession = getSession()) {
			try {
				currenciesRates = sqlSession.getMapper(CurrenciesRateMapper.class).selectByFromCurrencyId(id);
			} catch (RuntimeException ex) {
				sqlSession.rollback();
				throw ex;
			}
			sqlSession.commit();
		}
		
		return currenciesRates;
	}

}
