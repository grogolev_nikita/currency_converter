package com.thumbtack.converter.dao;

import org.apache.ibatis.session.SqlSession;

import com.thumbtack.converter.utils.MyBatisUtils;

public class BaseDao {
	protected static SqlSession getSession() {
		return MyBatisUtils.getSqlSessionFactory().openSession();
	}
}
