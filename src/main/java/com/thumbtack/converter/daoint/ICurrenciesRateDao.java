package com.thumbtack.converter.daoint;

import java.util.List;

import com.thumbtack.converter.dao.CurrenciesRateDao;
import com.thumbtack.converter.entities.CurrenciesRate;
import com.thumbtack.converter.entities.Currency;

public interface ICurrenciesRateDao {
	List<CurrenciesRate> getAll();

	void batchInsertOrUpdate(List<CurrenciesRate> rates);
}
