package com.thumbtack.converter.daoint;

import java.util.List;

import com.thumbtack.converter.entities.Currency;

public interface ICurrencyDao {
	List<Currency> getAll();

	void batchInsertOrUpdate(List<Currency> currencies);

	Currency getByIso(String iso);
}
