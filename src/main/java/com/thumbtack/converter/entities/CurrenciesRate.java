package com.thumbtack.converter.entities;

public class CurrenciesRate {
	private Integer id;
	
	private Currency from;
	
	private Currency to;
	
	private Double rate;

	
	public CurrenciesRate() {
		// TODO Auto-generated constructor stub
	}
	
	public CurrenciesRate(Integer id, Currency from, Currency to, Double rate) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.rate = rate;
	}

	public CurrenciesRate(Currency from, Currency to, Double rate) {
		this(0, from, to, rate);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Currency getFrom() {
		return from;
	}

	public void setFrom(Currency from) {
		this.from = from;
	}

	public Currency getTo() {
		return to;
	}

	public void setTo(Currency to) {
		this.to = to;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}
}
