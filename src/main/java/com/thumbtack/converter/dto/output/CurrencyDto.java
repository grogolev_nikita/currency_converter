package com.thumbtack.converter.dto.output;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CurrencyDto {
	@JsonIgnore
	private Integer id;
	
	private String iso;
	
	private String name;
	
	public CurrencyDto() {
		
	}

	public CurrencyDto(Integer id, String iso, String name) {
		super();
		this.id = id;
		this.iso = iso;
		this.name = name;
	}

	public CurrencyDto(String iso, String name) {
		this(0, iso, name);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIso() {
		return iso;
	}

	public void setIso(String iso) {
		this.iso = iso;
	}
}
