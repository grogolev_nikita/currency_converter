package com.thumbtack.converter.dto.output;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CurrenciesRateDto {
	@JsonIgnore
	private Integer id;
	
	private String from;
	
	private String to;
	
	private Double rate;

	
	public CurrenciesRateDto() {
		// TODO Auto-generated constructor stub
	}
	
	public CurrenciesRateDto(Integer id, String from, String to, Double rate) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.rate = rate;
	}

	public CurrenciesRateDto(String from, String to, Double rate) {
		this(0, from, to, rate);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Double getRate() {
		return rate;
	}

	public void setRatio(Double rate) {
		this.rate = rate;
	}
}