package com.thumbtack.converter.dto.output;

import java.util.List;

import com.thumbtack.converter.entities.CurrenciesRate;

public class ConversionPathDto {
	private String from;
	
	private String to;
	
	private Integer amount;
	
	private List<CurrenciesRateDto> ratesPath;
	
	private Double conversionResult;

	public ConversionPathDto() {
		
	}
	
	public ConversionPathDto(String from, String to, Integer amount, List<CurrenciesRateDto> ratesPath,
			Double conversionResult) {
		super();
		this.from = from;
		this.to = to;
		this.amount = amount;
		this.ratesPath = ratesPath;
		this.conversionResult = conversionResult;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public List<CurrenciesRateDto> getRatesPath() {
		return ratesPath;
	}

	public void setRatesPath(List<CurrenciesRateDto> ratesPath) {
		this.ratesPath = ratesPath;
	}

	public Double getConversionResult() {
		return conversionResult;
	}

	public void setConversionResult(Double conversionResult) {
		this.conversionResult = conversionResult;
	}
}
