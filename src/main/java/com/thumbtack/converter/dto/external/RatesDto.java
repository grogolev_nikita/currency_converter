package com.thumbtack.converter.dto.external;

import java.util.Map;

public class RatesDto {
	private String result;
	
	private Integer timestamp;

	private String from;
	
	private Map<String, Double> rates;

	private String error;
	
	public RatesDto() {
		
	}
	
	public RatesDto(String result, Integer timestamp, String from, Map<String, Double> rates) {
		super();
		this.result = result;
		this.timestamp = timestamp;
		this.from = from;
		this.rates = rates;
	}

	public Integer getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Integer timestamp) {
		this.timestamp = timestamp;
	}

	public Map<String, Double> getRates() {
		return rates;
	}

	public void setRates(Map<String, Double> rates) {
		this.rates = rates;
	}
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
