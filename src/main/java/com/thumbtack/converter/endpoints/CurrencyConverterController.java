package com.thumbtack.converter.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thumbtack.converter.dto.output.ConversionPathDto;
import com.thumbtack.converter.dto.output.CurrenciesRateDto;
import com.thumbtack.converter.dto.output.CurrencyDto;
import com.thumbtack.converter.exceptions.ConverterException;
import com.thumbtack.converter.services.CurrencyConverterService;

@RestController
@RequestMapping("/api/currency")
public class CurrencyConverterController {
	@Autowired
	private CurrencyConverterService converterService;
	
	@CrossOrigin
	@GetMapping("/rates")
	public List<CurrenciesRateDto> getLatestConversionRates(
			@RequestParam(value="base", required=false) String baseCurrencyIso) throws ConverterException {
		return converterService.getLatestCurrencyRates(baseCurrencyIso);
	}
	
	@CrossOrigin
	@GetMapping("/symbols")
	public List<CurrencyDto> getAllCurrencies() throws ConverterException {
		return converterService.getAllCurrencies();
	}
	
	@CrossOrigin
	@GetMapping("/convert")
	public CurrenciesRateDto getLatestConversionRatesForIsoPair(
			@RequestParam(value="from", required=true) String fromIso,
			@RequestParam(value="to", required=true) String toIso,
			@RequestParam(value="amount", required=false) Integer amount) {
		return converterService.getConversionRate(fromIso, toIso, amount);
	}
	
	@CrossOrigin
	@GetMapping("/optimal_conversion")
	public ConversionPathDto getOptimanConversionPath(
			@RequestParam(value="from", required=true) String fromIso,
			@RequestParam(value="to", required=true) String toIso,
			@RequestParam(value="amount", required=false) Integer amount,
			@RequestParam(value="hops", required=false) Integer hops) {
		return converterService.getOprimalConversionPath(fromIso, toIso, amount, hops);
	}
	
}
