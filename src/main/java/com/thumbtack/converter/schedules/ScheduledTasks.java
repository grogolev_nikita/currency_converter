package com.thumbtack.converter.schedules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsunsoft.http.HttpRequest;
import com.jsunsoft.http.HttpRequestBuilder;
import com.jsunsoft.http.ResponseDeserializer;
import com.jsunsoft.http.ResponseHandler;
import com.thumbtack.converter.dto.external.RatesDto;
import com.thumbtack.converter.dto.output.CurrenciesRateDto;
import com.thumbtack.converter.dto.output.CurrencyDto;
import com.thumbtack.converter.exceptions.ConverterException;
import com.thumbtack.converter.services.CurrencyConverterService;

@Component
public class ScheduledTasks {
	@Autowired
	private CurrencyConverterService converterService;

	// @Value(${externalApiRequestInterval})
	private static final int externalApiRequestInterval = 60 * 60 * 1000;

	private static String externalApiUrl;

	private static String externalApiAccessKey;

	static {
		externalApiUrl = "https://v3.exchangerate-api.com/bulk/";
		externalApiAccessKey = "64bb8f173588d9584fff0bca";
	}

	@Scheduled(fixedRate = externalApiRequestInterval)
	@PostConstruct
	private void updateCurrencyData() throws ConverterException { 
		/*
		List<CurrencyDto> currencies = converterService.getAllCurrencies();
		List<CurrenciesRateDto> currenciesRates = new ArrayList();
		for (CurrencyDto currency : currencies) {
			String currentBaseCurrency = currency.getIso();
			String ratesJson = createRatesRequest(currentBaseCurrency);
			RatesDto rates = parseRatesJsonResponse(ratesJson);

			if (rates.getError() == null) {
				currenciesRates.addAll(convertRatesDtoToCurrenciesRateDtoList(rates, currentBaseCurrency));
			}
		}
		
		updateConversionRatesTable(currenciesRates);
		
		*/
		rebuildCurrencyGraph();
	}
	
	private void rebuildCurrencyGraph() {
		converterService.rebuildCurrencyGraph();
	}

	private List<CurrenciesRateDto> convertRatesDtoToCurrenciesRateDtoList(RatesDto rates, String baseCurrencyIso) {
		List currenciesRates = new ArrayList();

		for (Entry<String, Double> entry : rates.getRates().entrySet()) {
			currenciesRates.add(new CurrenciesRateDto(baseCurrencyIso, entry.getKey(), entry.getValue()));
		}

		return currenciesRates;
	}

	private void updateConversionRatesTable(List<CurrenciesRateDto> currenciesRates) {
		converterService.updateRates(currenciesRates);
	}

	private RatesDto parseRatesJsonResponse(String json) throws ConverterException {
		ObjectMapper objectMapper = new ObjectMapper();

		try {
			RatesDto response = objectMapper.readValue(json, RatesDto.class);

			if (response.getError() != null) {
				if (!response.getError().equals("unknown-code"))
					throw new ConverterException("Unable to parse external api response");
			}

			return response;
		} catch (IOException e) {
			throw new ConverterException("Unable to parse external api response");
		}
	}

	private String createRatesRequest(String baseCurrency) throws ConverterException {
		HttpRequest<String> request = HttpRequestBuilder
				.createGet(externalApiUrl + externalApiAccessKey + "/" + baseCurrency, String.class)
				.responseDeserializer(ResponseDeserializer.ignorableDeserializer()).build();

		ResponseHandler<String> response = request.execute();

		if (response.getStatusCode() != 200)
			throw new ConverterException("External api incorrect status code response");

		return response.get();
	}
}
