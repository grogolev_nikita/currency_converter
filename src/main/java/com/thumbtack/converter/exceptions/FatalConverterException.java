package com.thumbtack.converter.exceptions;

public class FatalConverterException extends Exception {

	public FatalConverterException() {
		super();
	}

	public FatalConverterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public FatalConverterException(String message, Throwable cause) {
		super(message, cause);
	}

	public FatalConverterException(String message) {
		super(message);
	}

	public FatalConverterException(Throwable cause) {
		super(cause);
	}

}
